//
//  ViewController.swift
//  sdk-falla-bella-ios
//
//  Created by Daira Bezzato on 25/09/2020.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureURL()
    }
    
    private func configureURL() {
        guard let url = URL(string: "https://demosdesarrollos.com.ar/falabella/index.html")
        else {return}
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

extension ViewController: WKNavigationDelegate {}

